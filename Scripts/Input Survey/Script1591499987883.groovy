import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

connection = CustomKeywords.'Database.createConnection'()

WebUI.click(findTestObject('Object Repository/Logbook - Dashboard Page/a_Task'))

WebUI.click(findTestObject('Object Repository/Logbook - Dashboard Page/a_My Survey'))

WebUI.click(findTestObject('Object Repository/Logbook - Survey Page/txt_SurveyTitle', [('text') : surveyName]))

WebUI.switchToWindowIndex(1)

if (surveyName.equals('Tele Survey V.3 STS')) {
    data = CustomKeywords.'Database.getSurveyQuestion'(connection, '2006SVY000003')
	
	WebUI.click(findTestObject('Object Repository/Logbook - Survey Page/txt_NamaKonsumen'))

    WebUI.setText(findTestObject('Object Repository/Logbook - Survey Page/txt_NamaKonsumen'), nama_konsumen)
	
	WebUI.click(findTestObject('Object Repository/Logbook - Survey Page/txt_CustomerID'))

    WebUI.setText(findTestObject('Logbook - Survey Page/txt_CustomerID'), customer_id)
	
	WebUI.click(findTestObject('Object Repository/Logbook - Survey Page/txt_NomorKontrak'))

    WebUI.setText(findTestObject('Logbook - Survey Page/txt_NomorKontrak'), no_kontrak)
	
	WebUI.click(findTestObject('Object Repository/Logbook - Survey Page/txt_NoTelp'))

    WebUI.setText(findTestObject('Logbook - Survey Page/txt_NoTelp'), no_telp)
} else {
    data = CustomKeywords.'Database.getSurveyQuestion'(connection, '2006SVY000004')

    WebUI.click(findTestObject('Object Repository/Logbook - Survey Page/txt_NamaKonsumen'))

    WebUI.setText(findTestObject('Object Repository/Logbook - Survey Page/txt_NamaKonsumen'), nama_konsumen)
	
	WebUI.click(findTestObject('Object Repository/Logbook - Survey Page/txt_CustomerID'))

    WebUI.setText(findTestObject('Logbook - Survey Page/txt_CustomerID'), customer_id)
	
	WebUI.click(findTestObject('Object Repository/Logbook - Survey Page/txt_NoTelp'))

    WebUI.setText(findTestObject('Logbook - Survey Page/txt_NoTelp'), no_telp)
}

WebUI.click(findTestObject('Object Repository/Logbook - Survey Page/a_Next'))

for (i = 0; i < data.size; i++) {
    answerType = CustomKeywords.'Database.getQuestionAnswerType'(connection, data[i].question_id)

    //println("Tipe Jawaban Soal Ke - ${i} = ${answerType[0].question_type_id}")
    if (answerType[0].question_type_id.equals('DRPDWN')) {
        WebUI.check(findTestObject('Object Repository/Logbook - Survey Page/chk_Jawaban', [('no') : i + 1]))
		//println("Jawaban Pilihan Berhasil di Pilih")
    } else {
        WebUI.setText(findTestObject('Object Repository/Logbook - Survey Page/txt_Jawaban', [('no') : i + 1]), "No Comment Ke - ${i}")
		//println("Jawaban Text Berhasil di Input")
    }
}

CustomKeywords.'Database.closeConnection'(connection)

WebUI.click(findTestObject('Object Repository/Logbook - Survey Page/a_Finish'))

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

