<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Input Logbook</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c2012cbb-7a6a-4113-8d5c-3811b97e2734</testSuiteGuid>
   <testCaseLink>
      <guid>3834be32-0cc8-4766-92d2-b9f7e4e3fa87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1a91ae53-d95b-461d-8654-cd786101e00d</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>1-1</value>
         </iterationEntity>
         <testDataId>Data Files/Login</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>1a91ae53-d95b-461d-8654-cd786101e00d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>8174b58f-e464-4eb2-b1fe-19e1850774b2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>1a91ae53-d95b-461d-8654-cd786101e00d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>60d6e514-025b-4ff6-949a-93204f1b4d73</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e5137368-98ba-4520-bd46-1661d368d284</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Input Survey</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2b8901d2-9fd8-4ce3-95dc-cbb4078d850b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Survey</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>86fa6c91-768f-46f5-ac7d-43a627b12605</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7aace51a-6915-4083-ad17-e0a8ebe87258</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>414d3814-c50a-4c2e-8975-263698847b3b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>33718e1a-28e1-4d30-a0ed-800a41f0afac</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>2b8901d2-9fd8-4ce3-95dc-cbb4078d850b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Survey Name</value>
         <variableId>89c202c0-723e-4bfe-beb9-73d3beb33f5d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>2b8901d2-9fd8-4ce3-95dc-cbb4078d850b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Nama Konsumen</value>
         <variableId>425e9e7e-6fed-435b-ba2f-8bd312300fe7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>2b8901d2-9fd8-4ce3-95dc-cbb4078d850b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ID Customer</value>
         <variableId>7633ea00-1162-4fc1-8400-b86093c19ef2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>2b8901d2-9fd8-4ce3-95dc-cbb4078d850b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>No Kontrak</value>
         <variableId>577e901c-e9cf-4df5-9fdd-69c8b45e0884</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>2b8901d2-9fd8-4ce3-95dc-cbb4078d850b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>No Telp</value>
         <variableId>c94f73f5-b4e9-4922-b9c9-bbe9c863c8b5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>86f3e309-be41-4aab-b1f9-c775ffc0fde7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
