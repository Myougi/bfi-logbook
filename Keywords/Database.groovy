import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.microsoft.sqlserver.jdbc.SQLServerConnection as Connection
import java.sql.*
import groovy.sql.Sql
import com.google.gson.Gson
import com.katalon.plugin.keyword.connection.DBType as DBType
import com.katalon.plugin.keyword.connection.ResultSetKeywords

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class Database {
	Connection connection;
	String dbType;
	String urlConnection;
	String urlPort;
	String dbName;
	String dbUsername;
	String dbPassword;
	String query;
	def queryResults;
	ResultSet actorData;

	@Keyword
	createConnection(){
		//(DBType.sqlserver, 'testdbmini', '1433', 'BFIDB', 'qa.yogi', 'cGFzc3dvcmQ=')

		try {
			connection = CustomKeywords.'com.katalon.plugin.keyword.connection.DatabaseKeywords.createConnection'(DBType.sqlserver, 'testdbmini', '1433', 'AISDB', 'qa.yogi', 'cGFzc3dvcmQ=')

			if(connection != null){
				println("Connected to Database")
				return connection;
			}
			else {
				println("Not Connected to Database")
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
	}

	@Keyword
	getSurveyQuestion(koneksi, surveyID){
		query = "Select * From BISDB.logbook.ms_survey_question Where survey_id = '${surveyID}'"

		try {
			actorData = CustomKeywords.'com.katalon.plugin.keyword.connection.DatabaseKeywords.executeQuery'(koneksi, query);

			if(CustomKeywords.'com.katalon.plugin.keyword.connection.ResultSetKeywords.isEmptyResult'(actorData) == true) {
				println("Tidak ada data")
			} else {
				queryResults = CustomKeywords.'com.katalon.plugin.keyword.connection.ResultSetKeywords.resultSetToArrayList'(actorData);
				return queryResults;
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
	}

	@Keyword
	getQuestionAnswerType(koneksi, questionID){
		query = "Select question_type_id From BISDB.logbook.ms_question Where question_id = '${questionID}'"

		try {
			actorData = CustomKeywords.'com.katalon.plugin.keyword.connection.DatabaseKeywords.executeQuery'(koneksi, query);

			if(CustomKeywords.'com.katalon.plugin.keyword.connection.ResultSetKeywords.isEmptyResult'(actorData) == true) {
				println("Tidak ada data")
			} else {
				queryResults = CustomKeywords.'com.katalon.plugin.keyword.connection.ResultSetKeywords.resultSetToArrayList'(actorData);
				return queryResults;
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
	}

	@Keyword
	getQuestionAnswer(koneksi, questionID){
		query = "Select answer_description From BISDB.logbook.ms_answer Where question_id = '${questionID}'"

		try {
			actorData = CustomKeywords.'com.katalon.plugin.keyword.connection.DatabaseKeywords.executeQuery'(koneksi, query);

			if(CustomKeywords.'com.katalon.plugin.keyword.connection.ResultSetKeywords.isEmptyResult'(actorData) == true) {
				println("Tidak ada data")
			} else {
				queryResults = CustomKeywords.'com.katalon.plugin.keyword.connection.ResultSetKeywords.resultSetToArrayList'(actorData);
				return queryResults;
			}
		} catch (Exception e) {
			e.printStackTrace()
		}
	}

	@Keyword
	closeConnection(koneksi){
		CustomKeywords.'com.katalon.plugin.keyword.connection.DatabaseKeywords.closeConnection'(koneksi)
	}
}